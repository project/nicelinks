README.txt for nicelinks
========================

This module gives pleasant-looking DHTML CSS tooltips on
Internet Explorer & Gecko browsers (Mozilla, Netscape 7, etc.)

It knows the difference between internal and external links
(provided the internal ones are fully-qualified), and displays
the URL of the page at the bottom of the tooltip for external
links only.

Installation
============
 - Place nicelinks.module, nicelinks.js, and nicelinks.css in modules/nicelinks/
 - Enable the module as usual from Drupal's admin pages.

Configuration
=============
There is no configuration. It just works. :)
To turn it on and off, enable/disable the module.

This module can plug in to the glossary module (use glossary style href).

Bugs/comments/suggestions/patches to drupal-nicelinks@almaw.com


TODO: Support for Opera 7
      Configuration interface for changing the styles.
      (Opera 6 is untested - if it works for you, please e-mail me).
      Improve detection of internal links to make it work for
      relative links too.
