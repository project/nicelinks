
addEvent(window, "load", makeNiceTitles);

var browser = new Browser();
var lnk;
var d;
var domain;
var timeleft;

function makeNiceTitles() {
  if (!document.links || !document.createElement) return;

  var i, a, main;

  for (var ti=0;ti<document.links.length;ti++) {
    var lnk = document.links[ti];
    if (lnk.title) {
      lnk.setAttribute("nicetitle",lnk.title);
      lnk.removeAttribute("title");
      addEvent(lnk,"mouseover",showNiceTitle);
      addEvent(lnk,"mouseout",hideNiceTitle);
//      addEvent(lnk,"focus",showNiceTitle);
      addEvent(lnk,"blur",hideNiceTitle);
      addEvent(lnk,"mousemove", moveNiceTitle);
    }
  }

  var first_split = location.href.split("//");
  var without_resource = first_split[1];
  var second_split = without_resource.split("/");
  domain = first_split[0] + "//" + second_split[0];
}

function showNiceTitle(e) {

    if (!document.getElementsByTagName) return;
    if (d) {
        document.getElementsByTagName("body")[0].removeChild(d);
        d = null;
    }

    if (window.event && window.event.srcElement) {
        lnk = window.event.srcElement
    } else if (e && e.target) {
        lnk = e.target
    }
    if (!lnk) return;
    if (lnk.nodeType == 3) {
        // lnk is a textnode -- ascend parents until we hit a link
        lnk = getParent(lnk,"A");
    }
    if (!lnk) return;
    nicetitle = lnk.getAttribute("nicetitle");

    d = document.createElement("div");

    if (lnk.getAttribute("class") == "glossary_indicator" || lnk.getAttribute("class") == "glossary_term") {
      d.className = "niceglossary";
      var term = nicetitle.substr(0, nicetitle.indexOf(':'));
      var desc = nicetitle.substr(nicetitle.indexOf(':') + 1);
      tnu = document.createTextNode(term + ":");
      pau = document.createElement("p");
      pau.className = "title";
      pau.appendChild(tnu);
      d.appendChild(pau);
      tnt = document.createTextNode(desc);
      pat = document.createElement("p");
      pat.className = "defn";
      pat.appendChild(tnt);
      d.appendChild(pat);
    } else {
      d.className = "nicetitle";
      tnt = document.createTextNode(nicetitle);
      pat = document.createElement("p");
      pat.className = "text";
      pat.appendChild(tnt);
      d.appendChild(pat);
    }

    if (lnk.href.indexOf(domain) == -1) {
      tnd = document.createTextNode(lnk.href);
      pad = document.createElement("p");
      pad.className = "destination";
      pad.appendChild(tnd);
      d.appendChild(pad);
      h_pixels = lnk.href.length*6;
    } else {
      h_pixels = 0;
    }
    
    STD_WIDTH = 300;
    t_pixels = nicetitle.length*10;
    
    if (h_pixels > STD_WIDTH) {
        w = h_pixels;
    } else if ((STD_WIDTH>t_pixels) && (t_pixels>h_pixels)) {
        w = t_pixels;
    } else if ((STD_WIDTH>t_pixels) && (h_pixels>t_pixels)) {
        w = h_pixels;
    } else {
        w = STD_WIDTH;
    }

    d.style.width = w + 'px';
    tipfocus(e);
}

function tipfocus(e) {
  var mx;
  var my;
  var xy;

  xy = getMousePosition(e);
  mx = xy[0] + 20; my = xy[1] + 20;

//  mx = lnk.offsetLeft;
//  my = lnk.offsetTop;
  
  d.style.left = (mx) + 'px';
  d.style.top = (my) + 'px';

  if (document.body && document.body.offsetWidth && ((mx+w) > document.body.offsetWidth)) {
    d.style.left = (document.body.offsetWidth - w - 20) + "px";
  }   
  document.getElementsByTagName("body")[0].appendChild(d);
  timeleft = 80;
  countdown();
}

function countdown() {
  timeleft--;
  if (timeleft > 0)
    setTimeout("countdown()", 100);
  else
    hideNiceTitle(d);
}

function moveNiceTitle(e) {
  if (d) {
    timeleft = 80;
    xy = getMousePosition(e);
    mx = xy[0] + 20; my = xy[1] + 20;
   
    d.style.left = (mx) + 'px';
    d.style.top = (my) + 'px';

    if (document.body && document.body.offsetWidth && ((mx+w+20) > document.body.offsetWidth)) {
      d.style.left = (document.body.offsetWidth - w - 20) + "px";
    }   

    if (document.body && document.body.offsetHeight && ((my+75) > document.body.offsetHeight)) {
      d.style.top = (document.body.offsetHeight - 75) + "px";
    }   

  }
}

function hideNiceTitle(e) {
    if (!document.getElementsByTagName) return;
    if (d) {
        document.getElementsByTagName("body")[0].removeChild(d);
        d = null;
    }
}

// Add an eventListener to browsers that can do it somehow.
// Originally by the amazing Scott Andrew.
function addEvent(obj, evType, fn){
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, true);
    return true;
  } else if (obj.attachEvent){
	var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
	return false;
  }
}

function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}

function getMousePosition(event) {
  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }
  return [x,y];
}

// Determine browser and version.

function Browser() {
// blah, browser detect, but mouse-position stuff doesn't work any other way
  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}

